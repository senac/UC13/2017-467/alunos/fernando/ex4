/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author sala304b
 */
public class Pagamento {

    private HashMap<Integer , Double> comissoes = new HashMap<>();
    
    public Pagamento() {
    }
    
    private final double TAXA_COMISSAO = 0.05;
    
    public void calcularComissao(List<Venda> lista){
        
        for (Venda venda : lista){
            
            int codigo = venda.getVendedor().getCodigo();
            
            if(!comissoes.containsKey(codigo)){
                comissoes.put(codigo,venda.getTotal() * TAXA_COMISSAO) ; 
            }else{
               double comissaoParcial = comissoes.get(codigo).doubleValue() ; 
               comissaoParcial+= (venda.getTotal() * TAXA_COMISSAO) ; 
               comissoes.put(codigo, comissaoParcial);
            }  
        }
        }
    public double getComissaoVendedor(Vendedor vendedor){
        return this.comissoes.get(vendedor.getCodigo()).doubleValue() ; 
    }
}


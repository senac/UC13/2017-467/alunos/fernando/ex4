/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Produto;
import br.com.senac.Venda;
import br.com.senac.Vendedor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class VendaTest {

    public VendaTest() {
    }

    @Test
    public void deveSomaTotalNota() {

        Venda venda = new Venda(new Vendedor(1, "Daniel"));
        Produto produto = new Produto(1, "Sabao");
        venda.adicionarItem(produto, 1, 10);
        double resultado = venda.getTotal();

        assertEquals(resultado, 10, 0.05);

    }
}

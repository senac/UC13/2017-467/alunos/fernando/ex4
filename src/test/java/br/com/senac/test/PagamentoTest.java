/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Pagamento;
import br.com.senac.Produto;
import br.com.senac.Venda;
import br.com.senac.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class PagamentoTest {

    public PagamentoTest() {
    }

    @Test
    public void deveCalcularComissaoParaJoao() {

        Vendedor vendedor = new Vendedor(1, "Joao");
        Produto produto = new Produto(1, "Sabao");

        Venda venda1 = new Venda(vendedor);
        venda1.adicionarItem(produto, 1, 10);

        Venda venda2 = new Venda(vendedor);
        venda2.adicionarItem(produto, 1, 10);

        List<Venda> listaVendas = new ArrayList<>();
        listaVendas.add(venda1);
        listaVendas.add(venda2);

        Pagamento Pagamento = new Pagamento();
        Pagamento.calcularComissao(listaVendas);
        double comissao = Pagamento.getComissaoVendedor(vendedor);

        assertEquals(1, comissao, 0.01);
    }

    @Test
    public void deveCalcularComissaoParaJoaoEMiguel() {

        Vendedor joao = new Vendedor(1, "Joao");
        Vendedor miguel = new Vendedor(2, "Miguel");

        Produto produto = new Produto(1, "Sabao");

        Venda venda1 = new Venda(joao);
        venda1.adicionarItem(produto, 10, 10);

        Venda venda2 = new Venda(miguel);
        venda2.adicionarItem(produto, 10, 20);

        List<Venda> listaVendas = new ArrayList<>();
        listaVendas.add(venda1);
        listaVendas.add(venda2);

        Pagamento Pagamento = new Pagamento();
        Pagamento.calcularComissao(listaVendas);
        double comissaoJose = Pagamento.getComissaoVendedor(joao);

        assertEquals(5.0, comissaoJose, 0.01);
    }
}
